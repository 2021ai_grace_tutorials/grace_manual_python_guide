import json
import pickle
import pandas as pd
from data_preparation.data_prep_utils import encode_categories, encode_mush_labels

mush = pd.read_csv('raw_data/mushrooms.csv')
features = [x for x in mush.columns if x!='class']

with open('data_preparation/category_dictionaries/cat_dict.pickle', 'rb') as pickler:
    encoding_dict = pickle.load(pickler)

# features
mush_enc = encode_categories(
    mush[features].copy(), 
    encoding_dict
)

dtypes_dict = {
    'int8': 'INT',
    'numpy': 'INT', 
    'int16': 'INT', 
    'int32':,'INT',
    'int32': 'INT', 
    'int64': 'INT',
    'float16': 'FLOAT',
    'float32': 'FLOAT',
    'float64': 'FLOAT',
    'float96': 'FLOAT',
    'bool': 'BOOL',
    'str': 'STRING',
    'obj': 'STRING'
}

features_list = []
for x in mush_enc:
    dict = {}
    dict['name'] = x
    dict['dtype'] = dtypes_dict[mush_enc[x].dtype]
    dict['ftype'] = 'continuous'
    dict['range'] = [mush_enc[x].min(),
                     mush_enc[x].max()]
    features_list.append(dict)

# labels
mush_labels = encode_mush_labels(
    {'p': 1.0, 'e': 0.0}, 
    df=mush['class']
)

targets_list = [{
    'name': 'class',
    'dtype': str(mush_labels.dtype),
    'ftype': 'continious',
    'range': [mush_labels.min(),
              mush_labels.max()],
    'repeat': 1
}]

# contract dict
contract_dict = {
    'features': features_list,
    'targets': targets_list
}

with open('contract.json', 'w') as file:
    json.dump(contract_dict, file, indent=4)
