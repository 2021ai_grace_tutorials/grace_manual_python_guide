import pickle
import numpy as np
from xgboost import XGBClassifier
from data_preparation.data_prep_utils import encode_categories

class XGBClassifier_wrapper():

    def __init__(self, seed = np.random.randint(1,1000)):
            self.seed = seed

    def make_training(self, x_tr_enc, y_tr_enc, model_path, save=True):
        xgb_clf = XGBClassifier(random_state=self.seed)
        xgb_clf.fit(x_tr_enc, y_tr_enc)
        if save:
            with open(model_path, 'wb') as pickler:
                pickle.dump(xgb_clf, pickler)
        return xgb_clf

    @staticmethod
    def get_predictions(raw_data, encoding_dict_path, model_path):
        # get dictionary data
        with open(encoding_dict_path, 'rb') as pickler:
            encoding_dict = pickle.load(pickler)
        preproc_data = encode_categories(raw_data, encoding_dict)

        # upload the model
        with open(model_path, 'rb') as pickler:
            model = pickle.load(pickler)
        predictions = model.predict(preproc_data)
        return predictions
