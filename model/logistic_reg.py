import pickle
import numpy as np
from sklearn.linear_model import LogisticRegression
from data_preparation.data_prep_utils import encode_categories

class LogReg_wrapper():
    
    def __init__(self, seed = np.random.randint(1,1000)):
        self.seed = seed

    def make_trainig(self, x_tr_enc, y_tr_enc, model_path, save=True):
        log_reg = LogisticRegression(solver='liblinear',
                                    random_state=self.seed,
                                    fit_intercept=False,
                                    intercept_scaling=5)
        log_reg.fit(x_tr_enc, y_tr_enc)
        if save:
                with open(model_path, 'wb') as pickler:
                        pickle.dump(log_reg, pickler)
        return log_reg

    def get_predictions(self, raw_data, encoding_dict_path, model_path):
        # get dictionary data
        with open(encoding_dict_path, 'rb') as pickler:
                encoding_dict = pickle.load(pickler)

        # make preprosessing of input data
        preproc_data = encode_categories(raw_data, encoding_dict)

        # upload the model
        with open(model_path, 'rb') as pickler:
                model = pickle.load(pickler)

        # make predictions
        predictions = model.predict(preproc_data)
        return predictions