import re
import numpy as np

def model_comparer(list_of_class_instances,
                   list_of_pathes,
                   x_tr,
                   y_tr,
                   x_val,
                   y_val,
                   encoding_dict_path,
                   metric,
                   seed = False,
                   save=True):
    
    predictions_dict = {'metric': metric.__name__}

    # loop through wrappers with same method names
    for i in range(len(list_of_class_instances)):

        # initialize class instance
        class_instance = list_of_class_instances[i]

        # train model
        class_instance.make_trainig(
            x_tr,
            y_tr,
            model_path=list_of_pathes[i],
            save=save
        )
        
        # get predictions
        pred = class_instance.get_predictions(
            x_val,
            encoding_dict_path,
            model_path=list_of_pathes[i]
        )

        # add predictions to the dict
        name = type(class_instance).__name__
        name = re.sub(r"(_.*)", "", name)
        result = metric(y_val, pred)
        predictions_dict[name] = result

    return predictions_dict 