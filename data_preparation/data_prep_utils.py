import pickle
from definitions import ROOT_DIR
from sklearn.model_selection import train_test_split


def split_mushroom_data(df, class_column, seed):
    X = df.loc[:, df.columns != class_column]
    Y = df[class_column]
    x_tr, x_val, y_tr, y_val = train_test_split(X, Y, random_state=seed)
    print('Shape of training set is {} and training labels is {}.'.format(x_tr.shape, y_tr.shape))
    print('Shape of validation set is {} and validation labels is {}.'.format(x_val.shape, y_val.shape))
    print("")
    return x_tr, x_val, y_tr, y_val


def make_features_dicts(training_set, save=True):
    encoding_dict = {}
    for column in training_set:
        uniques = training_set[column].unique().tolist()
        codes = [float(num) for num in range(1, len(uniques) + 1)]
        encoding_dict[column] = dict(zip(uniques, codes))
    if save:
        with open(ROOT_DIR + '/data_preparation/category_dictionaries/cat_dict.pickle', 'wb') as pickler:
            pickle.dump(encoding_dict, pickler)
    return encoding_dict


def encode_categories(df, encoding_dict):
    df_enc = df.copy()
    for column in df_enc:
        df_enc[column] = df_enc[column].apply(lambda x: encoding_dict[column].get(x, 0))
    return df_enc


def encode_mush_labels(class_dict, df):
    df_enc = df.copy()
    return df_enc.replace(class_dict)
