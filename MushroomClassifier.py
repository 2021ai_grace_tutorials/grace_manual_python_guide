import pickle
import pandas as pd
from definitions import ROOT_DIR
from data_preparation.data_prep_utils import encode_categories

class MushroomClassifier(object):

    def __init__(self):
        with open(ROOT_DIR + '/trained_model/xgb_model.pickle', 'rb') as file:
            self.model = pickle.load(file)
        with open(ROOT_DIR + '/data_preparation/category_dictionaries/cat_dict.pickle', 'rb') as file:
            self.enc_dict = pickle.load(file)

    def predict(self, X, features_names):
        X_compiled = pd.DataFrame(X, columns=features_names)
        X_coded = encode_categories(X_compiled, encoding_dict=self.enc_dict)
        return self.model.predict(X_coded)
