import pandas as pd
from sklearn.metrics import accuracy_score
from definitions import ROOT_DIR
from model.xgb_wrapper import XGBClassifier_wrapper
from model.logistic_reg import LogReg_wrapper
from model.model_comparer import model_comparer

from data_preparation.data_prep_utils import (
    split_mushroom_data, make_features_dicts, encode_categories, encode_mush_labels
)

# lock seed
seed = 147

# Load dataset
mush = pd.read_csv(ROOT_DIR + '/raw_data/mushrooms.csv')

# split data
x_tr, x_val, y_tr, y_val = split_mushroom_data(
    df=mush,
    class_column='class',
    seed=seed
)

# make dictionary with codes for categorical values
encoding_dict = make_features_dicts(x_tr, save=True)

# encode categories
x_tr = encode_categories(df=x_tr, encoding_dict=encoding_dict)

# encode labels
y_tr = encode_mush_labels({'p': 1.0, 'e': 0.0}, df=y_tr)
y_val = encode_mush_labels({'p': 1.0, 'e': 0.0}, df=y_val)

# initialize instances with models
xgb_clf = XGBClassifier_wrapper(seed)
lr_clf = LogReg_wrapper(seed) 

# compare models
list_of_class_instances = [xgb_clf, 
                          lr_clf,]

list_of_pathes = [ROOT_DIR + '/trained_model/xgb_model.pickle',
                  ROOT_DIR + '/trained_model/lr_model.pickle',]

results = model_comparer(list_of_class_instances,
                        list_of_pathes,
                        x_tr,
                        y_tr,
                        x_val,
                        y_val,
                        encoding_dict_path=ROOT_DIR + '/data_preparation/category_dictionaries/cat_dict.pickle',
                        metric=accuracy_score,
                        seed=seed,
                        save=True)

for (key, value) in results.items():
    print(key, ":", value)
